﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StreamLinkApi.Command;

namespace StreamLinkApi.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class StreamsController : ControllerBase
	{
		private readonly ILogger<StreamsController> _logger;

		public StreamsController(ILogger<StreamsController> logger)
		{
			_logger = logger;
		}

		/// <summary>
		/// Get a streamable videos links for a stream.
		/// </summary>
		/// <param name="UrlEncoded">
		/// Full url to the stream.
		/// *Must be url encoded for this endpoint*
		/// </param>
		[HttpGet("/{UrlEncoded}")]
		[ProducesResponseType(typeof(StreamlinkResult), (int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		public Task<ActionResult<StreamlinkResult>> Get([FromRoute] string UrlEncoded) => GetStreamLinks(WebUtility.UrlDecode(UrlEncoded));

		/// <summary>
		/// Get a streamable videos links for a stream.
		/// </summary>
		/// <param name="url">Full url to the stream. Must be url encoded for this endpoint</param>
		[HttpGet("/")]
		[ProducesResponseType(typeof(StreamlinkResult), (int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		public Task<ActionResult<StreamlinkResult>> GetQuery([FromQuery] string url) => GetStreamLinks(url);

		private async Task<ActionResult<StreamlinkResult>> GetStreamLinks(string url)
		{
			try
			{
				var uri = new Uri(url, UriKind.Absolute);
				return await StreamLinksActionResult(uri);
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}
		}

		private async Task<ActionResult<StreamlinkResult>> StreamLinksActionResult(Uri uri)
		{
			StreamlinkResult streamLinks = await Streamlink.Run(uri);

			if (!streamLinks?.Streams?.Any() ?? true)
				return NoContent();

			return Ok(streamLinks);
		}
	}
}
