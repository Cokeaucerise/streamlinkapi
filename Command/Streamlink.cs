using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

namespace StreamLinkApi.Command
{
	public static class Streamlink
	{
		static readonly JsonSerializerOptions JsonOptions = new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

		public static async Task<StreamlinkResult> Run(Uri url)
		{
			try
			{
				return await RunStreamlink(url);
			}
			catch (Exception)
			{
				throw;
			}
		}

		private static async Task<StreamlinkResult> RunStreamlink(Uri url)
		{
			using Process process = CreateStreamlinkProcess(url);

			process.Start();
			string json = process.StandardOutput.ReadToEnd();

			await process.WaitForExitAsync();

			if (process.ExitCode != 0)
				throw new ArgumentException(JsonSerializer.Deserialize<StreamLinkError>(json, JsonOptions).Error);

			return JsonSerializer.Deserialize<StreamlinkResult>(json, JsonOptions);

		}

		private static Process CreateStreamlinkProcess(Uri url)
		{
			return new()
			{
				StartInfo = new ProcessStartInfo
				{
					WindowStyle = ProcessWindowStyle.Hidden,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					FileName = "/bin/sh",
					Arguments = @$"-c ""streamlink --json {url}""",
				}
			};
		}
	}

	public record StreamlinkResult
	{
		/// <summary>
		/// Name of the service
		/// </summary>
		/// <example>Youtube</example>
		public string Plugin { get; set; }

		/// <summary>
		/// Dictionary of the streams found.
		/// The key is the stream quality.
		/// </summary>
		public Dictionary<string, StreamResult> Streams { get; set; }

		public StreamlinkResult() { }
	}

	public record StreamResult
	{
		/// <summary>
		/// Encoding type
		/// </summary>
		/// <example>hls</example>
		public string Type { get; set; }

		/// <summary>
		/// Video stream link.
		/// </summary>
		/// <example>https://www.url.hls/da23a5d9526049b9978c3bb280ee2ccb.m3u8</example>
		public Uri Url { get; set; }

		/// <summary>
		/// Additional headers
		/// </summary>
		public Dictionary<string, string> Headers { get; set; }

		/// <summary>
		/// Video url source
		/// </summary>
		/// <example>https://www.url.hls/da23a5d9526049b9978c3bb280ee2ccb.m3u8</example>
		public Uri Master { get; set; }

		public StreamResult() { }
	}

	public record StreamLinkError
	{
		public string Error { get; set; }

		public StreamLinkError()
		{ }
	}
}